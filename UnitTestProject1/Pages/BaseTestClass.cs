﻿using System;
using System.Text;
using System.Collections.Generic;
using UITestsCommon;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace UITestsCommon.Pages
{
    [TestFixture]
    public class BaseTestClass
    {
        public BaseTestClass()
        {
        }

        //Every Test Will Run This Initialisation Method At The Start
        [SetUp]
        public void Initialize()
        {
            Console.WriteLine("Test has begun");
            Driver.LaunchBrowser();
        }

        //Every Test Will Run The Cleanup At The End
        [OneTimeTearDown]
        public void Cleanup()
        {
            Driver.Dispose();
            Console.WriteLine("Test has concluded");
        }
    }
}
