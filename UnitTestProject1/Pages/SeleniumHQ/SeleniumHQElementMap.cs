﻿using OpenQA.Selenium;

namespace UITestsCommon.Pages.SeleniumHQ
{
    public class SeleniumHQElementMap : BasePageElementMap
    {
        public IWebElement ProjectsBtn
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("menu_projects"), 30);
                return Browser.FindElement(By.Id("menu_projects"));
            }
        }

        public IWebElement DownloadBtn
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("menu_download"), 30);
                return Browser.FindElement(By.Id("menu_download"));
            }
        }

        public IWebElement DocumentationBtn
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("menu_documentation"), 30);
                return Browser.FindElement(By.Id("menu_documentation"));
            }
        }

        public IWebElement SupportBtn
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("menu_support"), 30);
                return Browser.FindElement(By.Id("menu_support"));
            }
        }

        public IWebElement AboutBtn
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("menu_about"), 30);
                return Browser.FindElement(By.Id("menu_about"));
            }
        }

        public IWebElement SearchInputField
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("q"), 30);
                return Browser.FindElement(By.Id("q"));
            }
        }

        public IWebElement GoBtn
        {
            get
            {
                Helpers.WaitForElementPresent(By.Id("submit"), 30);
                return Browser.FindElement(By.Id("submit"));
            }
        }
    }
}
