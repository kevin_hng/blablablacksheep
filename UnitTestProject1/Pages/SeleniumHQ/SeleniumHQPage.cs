﻿using OpenQA.Selenium;

namespace UITestsCommon.Pages.SeleniumHQ
{
    public class SeleniumHQPage : BasePage<SeleniumHQPage, SeleniumHQElementMap>
    {
        public void WaitForPromoBannerVisible()
        {
            Helpers.WaitForElementPresent(By.Id("promo"), 30);
        }

        //Menu Navigation
        public void SelectProjects()
        {
            Helpers.TryClickElement(this.Map.ProjectsBtn);
            WaitForPromoBannerVisible();
        }

        public void SelectDownload()
        {
            Helpers.TryClickElement(this.Map.DownloadBtn);
            WaitForPromoBannerVisible();
        }

        public void SelectDocumentation()
        {
            Helpers.TryClickElement(this.Map.DocumentationBtn);
            WaitForPromoBannerVisible();
        }

        public void SelectSupport()
        {
            Helpers.TryClickElement(this.Map.SupportBtn);
            WaitForPromoBannerVisible();
        }

        public void SelectAbout()
        {
            Helpers.TryClickElement(this.Map.AboutBtn);
            WaitForPromoBannerVisible();
        }

        public void ConductSearch(string s)
        {
            IWebElement input = this.Map.SearchInputField;

            Helpers.TryClickElement(input);
            input.SendKeys(s);
            Helpers.TryClickElement(this.Map.GoBtn);
            Helpers.WaitForElementPresent(By.XPath(".//div[2]"), 30);
        }

        public string GetSearchValue()
        {
            return Driver._webDriver.FindElement(By.Id("gsc-i-id1")).GetAttribute("value");
        }


    }
}
