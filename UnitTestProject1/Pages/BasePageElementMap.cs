﻿using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace UITestsCommon.Pages
{
    public class BasePageElementMap
    {
        protected IWebDriver Browser;
        protected WebDriverWait BrowserWait;

        public BasePageElementMap()
        {
            Browser = Driver._webDriver;
        }
    }
}
