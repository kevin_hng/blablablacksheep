﻿namespace UITestsCommon.Pages
{
    public abstract class BasePage<TS, TM> : ThreadSafeNestedContructorsBaseSingleton<TS>
    where TM : BasePageElementMap, new()
    where TS : BasePage<TS, TM>
    {
        protected TM Map
        {
            get
            {
                return new TM();
            }
        }

        protected void Navigate(string url = "")
        {
            Driver._webDriver.Navigate().GoToUrl(string.Concat(url));
        }
    }

    public abstract class BasePage<TS, TM, TV> : BasePage<TS, TM>
        where TM : BasePageElementMap, new()
        where TS : BasePage<TS, TM, TV>
        where TV : BasePageValidator<TS, TM, TV>, new()
    {
        public TV Validate()
        {
            return new TV();
        }
    }
}
