﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace UITestsCommon
{
    public static class Helpers
    {
        #region WaitHelpers
        public static void WaitForElementNotPresent(By locator, int secondsToWait = 30)
        {
            System.Collections.ObjectModel.ReadOnlyCollection<IWebElement> elemToDiscover = Driver._webDriver.FindElements(locator);
            while (elemToDiscover.Count >= 1)
            {
                new WebDriverWait(Driver._webDriver, new TimeSpan(0, 0, secondsToWait))
                   .Until(d => d.FindElements(locator).Count == 0
                       );

                elemToDiscover = Driver._webDriver.FindElements(locator);
            }
        }

        public static void WaitForElementPresent(By locator, int secondsToWait = 30)
        {
            new WebDriverWait(Driver._webDriver, TimeSpan.FromSeconds(secondsToWait)).Until(ExpectedConditions.ElementIsVisible(locator));
        }

        #endregion

        #region ClickHelpers
        public static void TryFindandClickElement(By locator)
        {
            var elemToClick = Driver._webDriver.FindElement(locator);
            TryClickElement(elemToClick);
        }

        public static void TryClickElement(IWebElement element)
        {
            Actions action = new Actions(Driver._webDriver);
            action.MoveToElement(element).Click().Perform();
        }
        #endregion

        public static void TakeScreenshot()
        {
            Screenshot ss = ((ITakesScreenshot)Driver._webDriver).GetScreenshot();
            ss.SaveAsFile("C:/ChromeDriver/TestRun" + DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss") + ".jpg", ScreenshotImageFormat.Jpeg);
        }

        public static void NavigateToURL(string url)
        {
            Driver._webDriver.Navigate().GoToUrl(url);
        }
    }
}
