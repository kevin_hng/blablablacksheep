﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;

namespace UITestsCommon
{
    public static class Driver
    {

        public static string CommonPath = ConfigurationManager.AppSettings["FirstKey"];

        public class DriverVariables
        {
            public string UITestsCommonPath { get; set; }
        }

        static DriverVariables _variables = JsonConvert.DeserializeObject<DriverVariables>(File.ReadAllText(CommonPath+"Variables.json"));

        //public static string CommonPath = _variables.UITestsCommonPath;

        

        //ChromeDriver executable location
        public static readonly string ChromeDriverDirectory = "C:/ChromeDriver";

        //WebDriver Initialisation
        public static IWebDriver _webDriver = null;
        public static IWait<IWebDriver> _webDriverWait;

        public static void LaunchBrowser()
        {
            CommonPath = _variables.UITestsCommonPath;
            var options = new ChromeOptions();
            options.AddArguments("--incognito");
            _webDriver = new ChromeDriver(ChromeDriverDirectory, options);
            _webDriverWait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(10));
        }

        //End WebDriver Instance
        public static void Dispose()
        {
            _webDriver.Dispose();
        }
    }
}
