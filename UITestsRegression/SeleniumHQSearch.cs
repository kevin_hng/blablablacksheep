﻿using System;
using UITestsCommon.Pages;
using UITestsCommon.Pages.SeleniumHQ;
using UITestsCommon;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UITestsRegression
{
    [TestClass]
    public class SeleniumHQSearch : BaseTestClass
    {
        [TestMethod]
        [TestCategory("Regression")]
        public void UI_Regression_SeleniumHQSearch()
        {
            try
            {
                //Arrange ==============================================================
                string searchQuery = "about";
                string url = "http://www.seleniumhq.org/";
                //Act ==================================================================
                Helpers.NavigateToURL(url);
                SeleniumHQPage hqPage = new SeleniumHQPage();
                hqPage.ConductSearch(searchQuery);
                //Assert ===============================================================
                Assert.IsTrue(hqPage.GetSearchValue().Contains(searchQuery), "Expecting actual search query to equal desired search query");
            }
            catch(System.Exception e)
            {
                Helpers.TakeScreenshot();
                Assert.Fail("" + e);
            }
        }
    }
}
