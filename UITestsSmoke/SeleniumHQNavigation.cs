﻿using UITestsCommon;
using UITestsCommon.Pages;
using UITestsCommon.Pages.SeleniumHQ;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UITestsSmoke
{
    [TestClass]
    public class SeleniumHQNavigation : BaseTestClass
    {
        [TestMethod]
        [TestCategory("Smoke")]
        public void UI_Smoke_SeleniumHQNavigation()
        {
            try
            {
                //Arrange ==============================================================
                string url = "http://www.seleniumhq.org/";
                //Act ==================================================================
                Helpers.NavigateToURL(url);
                SeleniumHQPage hqPage = new SeleniumHQPage();
                hqPage.SelectProjects();
                hqPage.SelectDownload();
                hqPage.SelectDocumentation();
                hqPage.SelectSupport();
                hqPage.SelectAbout();
                //Assert ===============================================================
            }
            catch(System.Exception e)
            {
                Helpers.TakeScreenshot();
                Assert.Fail("" + e);
            }
        }
    }
}
