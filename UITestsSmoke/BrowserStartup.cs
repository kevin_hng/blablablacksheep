﻿using UITestsCommon.Pages;
using UITestsCommon;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

namespace UITestsSmoke
{
    [TestFixture]
    public class BrowserStartup : BaseTestClass
    {
        [Test]
        [Category("Smoke")]
        public void UI_Smoke_BrowserStartup()
        {
            try
            {
                //Arrange ==============================================================
                string url= "https://www.seleniumhq.org/";
                //Act ==================================================================
                Helpers.NavigateToURL(url);
                //Assert ===============================================================
                Assert.IsTrue(Driver._webDriver.Url.Contains(url), "Expecting actual URL to equal desired URL");
            }
            catch (System.Exception e)
            {
                Helpers.TakeScreenshot();
                Assert.Fail("" + e);
            }
        }
    }
}
